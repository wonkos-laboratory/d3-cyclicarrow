# d3-foo

A d3 Plugin to create Cyclic Arrow Diagrams. A Prominent Example for this type of Diagram is the DevOps Infinity-Loop.

## Installing

If you use NPM, `npm install d3-cyclicarrow`. Otherwise, download the [latest release](https://github.com/d3/d3-foo/releases/latest).

## API Reference

TODO

<a href="#foo" name="foo">#</a> <b>foo</b>()

Computes the answer to the ultimate question of life, the universe, and everything.
