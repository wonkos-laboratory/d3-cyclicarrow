import constant from "./constant.js";


export default function () {
  //var value =  
  //TODO implement the ability for arrow length bsed on value like in d3.pie

  function cyclicarrow(data) {
    var fractions = data.length,
      seg = 1 / fractions,
      segments = new Array(fractions);


    for (var i = 0; i < fractions; i++) {
      segments[i] = {
        index: i,
        value: data[i],
        start: i * seg,
        end: (i + 1) * seg
      }
    }
    return segments;
  }

  return cyclicarrow;
}

